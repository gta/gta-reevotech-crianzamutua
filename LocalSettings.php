<?php
# ----
# Copyright (C) 2013-2020 - REEVOTech (http://tech.reevo.org)
# License: Affero GPL version 3 - http://www.gnu.org/licenses/agpl.html
# ----

include('/srv/reevotech/etc/global_config-gta.php');


if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$wgMetaNamespace = "GTA";
$wgScriptPath = "";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;
$wgScriptPath	    = "";
$wgArticlePath      = "/$1";
$wgUsePathInfo      = true;
$wgResourceBasePath = $wgScriptPath;


## Database settings
$wgDBtype = "mysql";
$wgDBserver = "localhost";
$wgDBname = $REEVO_DB_WIKI;
$wgDBuser = $REEVO_DB_USER;
$wgDBpassword = $REEVO_DB_PASS;
$wgDBprefix = "";
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";
$wgDBmysql5 = false;


## Registro y perfil de usuarios
$wgHiddenPrefs[] = 'realname';
$wgHiddenPrefs[] = 'genre';
$wgGroupPermissions['*']['createaccount'] = true;
$wgRedirectOnLogin = 'Special:MyPage';

## Permisos
// $wgEmailConfirmToEdit = true;
// $wgGroupPermissions['*']['edit'] = false;
// $wgGroupPermissions['*']['createpage'] = true;
$wgGroupPermissions['user']['edit'] = true;
$wgGroupPermissions['user']['upload_by_url'] = true;


## Email
$wgEmergencyContact = "webmaster@$REEVO_URL";
$wgPasswordSender = "webmaster@$REEVO_URL";
$wgEnableEmail = true;
$wgEnableUserEmail = true; # UPO
$wgEnotifUserTalk = false; # UPO
$wgEnotifWatchlist = false; # UPO
$wgEmailAuthentication = true;

## Cache
$wgMainCacheType = CACHE_ACCEL;
$wgMemCachedServers = [];


## Subidas e imagenes
$wgEnableUploads = true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgAllowImageTag = true;
// $wgAllowExternalImages = true;
$wgUseInstantCommons = false;
$wgDiff3 = "/usr/bin/diff3";
$wgAllowCopyUploads = true;


## Idioma
$wgShellLocale = "C.UTF-8";
$wgLanguageCode = "en";
$wgMessagesDirs['GTA'] = "$IP/deployments/$REEVO_CODENAME/i18n";
$wgMessagesDirs['Reevo'] = "../../i18n/wiki/reevo";
$wgMessagesDirs['ISO'] = "../../i18n/wiki/iso";

## Licencia
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "https://creativecommons.org/licenses/by-sa/4.0/";
$wgRightsText = "Creative Commons Atribución-CompartirIgual";
$wgRightsIcon = "$wgResourceBasePath/resources/assets/licenses/cc-by-sa.png";


## Namespaces
// define("NS_AUDIOVISUAL", 3000); // Este número DEBE ser par.
// define("NS_AUDIOVISUAL_TALK", 3001); // Éste DEBE ser el siguiente entero impar.
// $wgExtraNamespaces[NS_AUDIOVISUAL] = "Audiovisual";
// $wgExtraNamespaces[NS_AUDIOVISUAL_TALK] = "Audiovisual_talk";
// $wgContentNamespaces[] = NS_AUDIOVISUAL;
// $smwgNamespacesWithSemanticLinks[NS_AUDIOVISUAL] = true;
// $wgNamespaceAliases['Pelicula'] = NS_AUDIOVISUAL;
// $wgNamespaceAliases['Pelicula_talk'] = NS_AUDIOVISUAL_TALK;
// $wgNamespacesWithSubpages[NS_AUDIOVISUAL] = true;

// define("NS_PRENSA", 3002); // Este número DEBE ser par.
// define("NS_PRENSA_TALK", 3003); // Éste DEBE ser el siguiente entero impar.
// $wgExtraNamespaces[NS_PRENSA] = "Prensa";
// $wgExtraNamespaces[NS_PRENSA_TALK] = "Prensa_talk";
// $wgContentNamespaces[] = NS_PRENSA;
// $smwgNamespacesWithSemanticLinks[NS_PRENSA] = true;
// $wgNamespacesWithSubpages[NS_PRENSA] = true;

define("NS_ENDORSER", 3004); // Este número DEBE ser par.
define("NS_ENDORSER_TALK", 3005); // Éste DEBE ser el siguiente entero impar.
$wgExtraNamespaces[NS_ENDORSER] = "Endorser";
$wgExtraNamespaces[NS_ENDORSER_TALK] = "Endorser_talk";
$wgContentNamespaces[] = NS_ENDORSER;
$smwgNamespacesWithSemanticLinks[NS_ENDORSER] = true;
$wgNamespacesWithSubpages[NS_ENDORSER] = true;

// define("NS_REVISAR", 3006); // Este número DEBE ser par.
// define("NS_REVISAR_TALK", 3007); // Éste DEBE ser el siguiente entero impar.
// $wgExtraNamespaces[NS_REVISAR] = "Revisar";
// $wgExtraNamespaces[NS_REVISAR_TALK] = "Revisar_talk";
// $wgContentNamespaces[] = NS_REVISAR;
// $smwgNamespacesWithSemanticLinks[NS_REVISAR] = true;
// $wgNamespacesWithSubpages[NS_REVISAR] = true;
//
// define("NS_EVENTO", 3008); // Este número DEBE ser par.
// define("NS_EVENTO_TALK", 3009); // Éste DEBE ser el siguiente entero impar.
// $wgExtraNamespaces[NS_EVENTO] = "Evento";
// $wgExtraNamespaces[NS_EVENTO_TALK] = "Evento_talk";
// $wgContentNamespaces[] = NS_EVENTO;
// $smwgNamespacesWithSemanticLinks[NS_EVENTO] = true;
// $wgNamespacesWithSubpages[NS_EVENTO] = true;

// $wgNamespaceProtection[NS_MAIN]     = $wgNamespaceProtection[NS_USER]  =
// $wgNamespaceProtection[NS_PROJECT]  = $wgNamespaceProtection[NS_IMAGE] =
// $wgNamespaceProtection[NS_TEMPLATE] = $wgNamespaceProtection[NS_HELP]  =
// $wgNamespaceProtection[NS_CATEGORY] = $wgNamespaceProtection[NS_AUDIOVISUAL] =
// $wgNamespaceProtection[NS_AUDIOVISUAL] = $wgNamespaceProtection[NS_ENDORSER] =
// $wgNamespaceProtection[NS_EVENTO] =
// $wgNamespaceProtection[NS_PRENSA] = array( 'autoconfirmed' );

$wgNamespaceProtection[NS_ENDORSER] = array( 'autoconfirmed' );

// $wgGroupPermissions['*']['revisar-edit'] = true;
// $wgNamespaceProtection[NS_REVISAR] = array( 'revisar-edit' );

$wgNamespacesToBeSearchedDefault = [
	// NS_MAIN => true,
	// NS_CATEGORY => true,
	// NS_USER => true,
	NS_ENDORSER => true
	// NS_PRENSA => true,
	// NS_AUDIOVISUAL => true,
	// NS_EVENTO => true
];

$wgNamespaceAliases = array(
	// 'U' => NS_USER,
	'E' => NS_ENDORSER,
	// 'P' => NS_PRENSA,
	// 'A' => NS_AUDIOVISUAL,
	// 'V' => NS_EVENTO
);


###### Skin y visuales

## Default skin
// wfLoadSkin( 'Vector' );
// wfLoadSkin( 'chameleon' );
$wgDefaultSkin = 'chameleon';
$wgLogo = "/deployments/$REEVO_CODENAME/logo.svg";
$egChameleonLayoutFile = $IP . '/deployments/'. $REEVO_CODENAME .'/layout.xml';
$wgRawHtml = true;

$egChameleonExternalStyleModules = [
	$IP . '/skins/bo/assets/fontawesome/css/all.css' => $wgScriptPath, // (ya NO se carga mediante JS en /MediaWiki:Common.js)
	$IP . '/skins/bo/bootswatch.less' => $wgScriptPath,
	$IP . '/skins/bo/variables.less' => $wgScriptPath,
  $IP . '/skins/bo/bo.less' => $wgScriptPath,
	$IP . '/skins/bo/reevo.less' => $wgScriptPath,
	$IP . '/deployments/'. $REEVO_CODENAME .'/style.less' => $wgScriptPath,
	$IP . '/deployments/'. $REEVO_CODENAME .'/fonts.css' => $wgScriptPath
	];

\Bootstrap\BootstrapManager::getInstance()->addCacheTriggerFile( $IP . '/deployments/'. $REEVO_CODENAME .'/style.less' );
\Bootstrap\BootstrapManager::getInstance()->addCacheTriggerFile( $IP . '/deployments/'. $REEVO_CODENAME .'/fonts.css' );


$egChameleonExternalLessVariables = [
	'font-size-base' => '16px',
	'font-size-large' => '18px',
	'font-size-small' => '10px',
	'font-size-h1' => '22px',
	'font-size-h2' => '20px',
	'font-size-h3' => '18px',
	'line-height-base' => '1.5',
	'navbar-margin-bottom' => '10px',
	'navbar-default-bg' => 'rgba( 94, 157, 200, 1 )',
	'navbar-default-border' => 'rgba( 94, 157, 200, 1 )',
	'nav-tabs-active-link-hover-color' => 'rgba( 255, 255, 255, 1 )',
	'input-border-focus' => 'rgba(0, 0, 0, .1)'
	];


###### Extensiones

## NoTitle
require_once "$IP/extensions/NoTitle/NoTitle.php";
$wgRestrictDisplayTitle = false;

## Cite
wfLoadExtension( 'Cite' );

## Semantic Mediawiki
// require_once "$IP/extensions/SemanticMediaWiki/SemanticMediaWiki.php";
enableSemantics( $REEVO_URL );
// $smwgQMaxInlineLimit = 2000;
wfLoadExtension( 'SemanticResultFormats' );
wfLoadExtension( 'SemanticFormsSelect' );
$smwgQMaxInlineLimit = 2000;

## ModernTimeline
wfLoadExtension( 'ModernTimeline' );

## PageForms
wfLoadExtension( 'PageForms' );

## Maps
wfLoadExtension( 'Maps' );
require_once $IP . '/extensions/Maps/Maps_Settings.php';

## ParserFunctions
wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;

## RegexFunctions
require_once "$IP/extensions/RegexFunctions/RegexFunctions.php";

## EmbedVideo
wfLoadExtension( 'EmbedVideo' );

## ExternalData
require_once "$IP/extensions/ExternalData/ExternalData.php";

## DataTrasnfer
wfLoadExtension( 'DataTransfer' );
include_once "$IP/extensions/DataTransfer/DataTransfer.php";

## WikiEditor
wfLoadExtension( 'WikiEditor' );
$wgDefaultUserOptions['usebetatoolbar'] = 1;

# MixedNamespaceSearchSuggestions
wfLoadExtension( 'MixedNamespaceSearchSuggestions' );

## LanguageTag
// require_once "$IP/extensions/LanguageTag/LanguageTag.php";
// $wgLanguageTagLanguages = array( "en", "es", "pt" );
//
// ## UniversalLanguageSelector
// wfLoadExtension( 'UniversalLanguageSelector' );
// $wgULSAcceptLanguageList = $wgLanguageTagLanguages;
// $wgULSIMEEnabled = false;
// $wgULSPosition = "interlanguage";

## Translate
// include_once "$IP/extensions/Translate/Translate.php";
// $wgGroupPermissions['user']['translate'] = true;
// // $wgGroupPermissions['translator']['skipcaptcha'] = true; // Bug 34182: needed with ConfirmEdit
// $wgTranslateDocumentationLanguageCode = 'qqq';
// $wgGroupPermissions['sysop']['pagetranslation'] = true;

// $wgPageLanguageUseDB = true;
// $wgGroupPermissions['user']['pagelang'] = true;
//
// $wgTranslateCC['wiki-sidebar'] = function( $id ) {
//         $mg = new WikiMessageGroup( $id, 'sidebar-messages' );
//         $mg->setLabel( 'Sidebar' );
//         $mg->setDescription( 'Messages used in the sidebar of this wiki' );
//         return $mg;
// };

// wfLoadExtension( 'SimpleLanguageSelector' );
// $wgSimpleLangageSelectionLangList = ['es', 'en', 'pt'];
// $wgSimpleLangageSelectionShowTranslateLink = true;

## EmailLogin
wfLoadExtension( 'EmailLogin' );

## GetUserName
require_once "$IP/extensions/GetUserName/GetUserName.php";

## AbuseFilter
wfLoadExtension( 'AbuseFilter' );
$wgGroupPermissions['sysop']['abusefilter-modify'] = true;
$wgGroupPermissions['*']['abusefilter-log-detail'] = true;
$wgGroupPermissions['*']['abusefilter-view'] = true;
$wgGroupPermissions['*']['abusefilter-log'] = true;
$wgGroupPermissions['sysop']['abusefilter-private'] = true;
$wgGroupPermissions['sysop']['abusefilter-modify-restricted'] = true;
$wgGroupPermissions['sysop']['abusefilter-revert'] = true;

## Nuke
wfLoadExtension( 'Nuke' );

## BlockBatch
require_once "$IP/extensions/BlockBatch/BlockBatch.php";

## OpenGraphMeta
wfLoadExtension( 'OpenGraphMeta' );

## Description2
wfLoadExtension( 'Description2' );
$wgEnableMetaDescriptionFunctions = true;

## HideNamespace
require_once "$IP/extensions/HideNamespace/HideNamespace.php";
$wgHidensNamespaces = array(
	// NS_PRENSA,
	// NS_AUDIOVISUAL,
	// NS_USER,
	NS_ENDORSER,
	// NS_EVENTO,
	// NS_REVISAR,
);

## CSS
wfLoadExtension( 'CSS' );

## MultimediaViewer
wfLoadExtension( 'MultimediaViewer' );

## UploadWizard

wfLoadExtension( 'UploadWizard' );
$wgApiFrameOptions = 'SAMEORIGIN';
$wgUploadWizardConfig = array(
	'debug' => false,
	'autoAdd' => array(
	 	'wikitext' => array(
			'This file was uploaded with the UploadWizard extension.'
			),
	 	'categories' => array(
	 		 'Uploaded with UploadWizard'
	 		 ),
		), // Should be localised to the language of your wiki instance
	'feedbackPage' => 'Feedback about UploadWizard',
	'altUploadForm' => 'Special:Upload',
	'fallbackToAltUploadForm' => false,
	'feedbackLink' => false, // Disable the link for feedback (default: points to Commons)
	'alternativeUploadToolsPage' => false, // Disable the link to alternative upload tools (default: points to Commons)
	'enableFormData' => true, // Enable FileAPI uploads be used on supported browsers
	'enableMultipleFiles' => true,
	'enableMultiFileSelect' => true,
	// 'uwLanguages' => array(
	// 	'ar' => 'العربية',
	// 	'de' => 'Deutsch',
	// 	'en' => 'English'
	// 	), // Selectable languages for file descriptions - defaults to 'en'
	'tutorial' => array(
	 	'skip' => true
		), // Skip the tutorial
	// 'maxUploads' => 15, // Number of uploads with one form - defaults to 50
	'fileExtensions' => $wgFileExtensions // omitting this may cause errors
	);

## MsUpload
wfLoadExtension( 'MsUpload' );
$wgMSU_useDragDrop = true; // Should the drag & drop area be shown? (Not set by default)
$wgMSU_showAutoCat = true; // Files uploaded while editing a category page will be added to that category
$wgMSU_checkAutoCat = true; // Whether the checkbox for adding a category to a page is checked by default
$wgMSU_useMsLinks = true; // Insert links in Extension:MsLinks style?
$wgMSU_confirmReplace = true; // Show the "Replace file?" checkbox
$wgMSU_imgParams = 'thumb|none'; // Any image parameter separated by a semicolon. Examples are: {width}px, left, right, center, none, border, frameless, frame, thumb etc etc

## ConfirmEdit - ANTISPAM!
wfLoadExtensions([ 'ConfirmEdit', 'ConfirmEdit/QuestyCaptcha' ]);

$wgCaptchaQuestions = [
    "es" => [
			'¿Cuál es la Capital de Francia?' => 'Paris',
			'¿De qué país es la capital Buenos Aires?' => 'Argentina', // Answers are case insensitive
			'¿Cuál es el nombre de esta página?' => [ 'Crianza Mutua' ], // A question may have many answers
			'¿Cuál es el nombre del único satélite natural de la Tierra?' => [ 'Luna', 'La Luna' ], // A question may have many answers
		 ],
		 "en" => [
 			'What is the name of the Capital of France?' => 'Paris',
 			'Buenos Aires is the capital city of...' => 'Argentina', // Answers are case insensitive
 			'What is the name of this website?' => [ 'Crianza Mutua' ], // A question may have many answers
 			'What is the name of the natural satellite of the Earth' => [ 'Moon', 'The Moon' ], // A question may have many answers
		],
		"pt" => [
		 'Qual é o nome da capital da França?' => 'Paris',
		 'De que país é a capital Buenos Aires?' => 'Argentina', // Answers are case insensitive
		 'Qual é o nome desta página da web?' => [ 'Crianza Mutua' ], // A question may have many answers
		 'Qual é o nome do único satélite natural na Terra?' => [ 'Lua', 'La lua' ], // A question may have many answers
		]
];

$wgCaptchaTriggers['edit']          = false;
// $wgCaptchaTriggersOnNamespace[NS_REVISAR]['create'] = true;
$wgCaptchaTriggers['createtalk']    = false;
$wgCaptchaTriggers['addurl']        = false;
$wgCaptchaTriggers['createaccount'] = true;
$wgCaptchaTriggers['badlogin']      = true;


## HideEmptySections
require_once( "$IP/extensions/HideEmptySections/HideEmptySections.php" );

## NewUserNotif
require_once "$IP/extensions/NewUserNotif/NewUserNotif.php";
$wgNewUserNotifEmailTargets = array(
    'info2019@reevo.org'
);

## ContactPage
$wgUserEmailUseReplyTo = true;
wfLoadExtension( 'ContactPage' );
$wgContactConfig['default'] = array(
	'RecipientUser' => 'Reebot', // Must be the name of a valid account which also has a verified e-mail-address added to it.
	'SenderName' => 'Contact Form on ' . $wgSitename, // "Contact Form on" needs to be translated
	'SenderEmail' => null, // Defaults to $wgPasswordSender, may be changed as required
	'RequireDetails' => true, // Either "true" or "false" as required
	'IncludeIP' => true, // Either "true" or "false" as required
	'MustBeLoggedIn' => false, // Check if the user is logged in before rendering the form
	'AdditionalFields' => array(
		'Text' => array(
			'label-message' => 'emailmessage',
			'type' => 'textarea',
			'rows' => 20,
			'required' => true,  // Either "true" or "false" as required
		),
	),
        // Added in MW 1.26
	'DisplayFormat' => 'table',  // See HTMLForm documentation for available values.
	'RLModules' => array(),  // Resource loader modules to add to the form display page.
	'RLStyleModules' => array(),  // Resource loader CSS modules to add to the form display page.
);
$wgCaptchaTriggers['contactpage'] = true;

$wgHooks['EmailUser'][] = function ( &$address, &$from, &$subject, &$text, &$error ) {
	global $wgPasswordSender;
	$subject = '[reevo.wiki] Contacto: ' . $subject;
	$address->address = 'info2019@reevo.org';
#	$from->address = $wgPasswordSender;
};


###### Extensiones propias

## CountryStates
wfLoadExtension( 'CountryStates' );

$wgShowExceptionDetails = true;
